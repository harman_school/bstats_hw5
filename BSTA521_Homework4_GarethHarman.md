# Harman HW5



**Notes:**
- <span style="color:darkred">Text = assignment descriptions</span>
- Text = my answers


look at d as an average
significant if 0 not in 95% CI

<span style="color:darkred"> 1. For the purpose of this exercise, use OR as the effect measure, and obtain a combined OR across all studies (including studies reporting sepsis and other infections) using a Bayesian hierarchical model and vague priors.</span> 

<span style="color:darkred">Based on the combined OR, is IIT effective to reduce the infection? Is it still possible for a new study to generate a non-significant estimate of OR? Could you think of a way to calculate the probability that ITT does not reduce infection?</span> 

<span style="color:darkred">Provide both the equal tail and HPD 95% CrIs for OR. Do you see any appreciable difference between the two CrIs in this case?</span>

OR new has a wider CI because it includes the variability in the posterior estimates of the other parameters such as d and tau.
