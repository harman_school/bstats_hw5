//
// This Stan program defines the model for the meta-analysis of beta blocker trials 

data {
  int<lower=0> Num;
  int n[Num, 2];  // num cases,
  int r[Num, 2];  // num of success
  int trt[Num, 2]; // num of treatment
}

parameters { 
  real mu[Num];         // Effect from the control group
  real delta[Num];      // Difference between treatmet groups in each study
  real d;               // mean treatment effect
  real<lower=0> tau;    // deviation of treatment effects
}
transformed parameters {
  real<lower=0> sigma;
  sigma = sqrt(tau);
}
model {
  d ~ normal(0.0,1000);  // vague prior
	tau ~ inv_gamma(0.001,0.001); // vague prior
  //sigma ~ cauchy(0, 5);  // A different way to specify an value prior for SD 

  for (i in 1:Num){
    for(j in 1:2)
	     r[i,j] ~ binomial_logit(n[i,j], mu[i]+delta[i]*trt[i,j]);
	     
  mu[i] ~ normal(0.0, 1000);
	delta[i] ~ normal(d, sigma);
  }
}
generated quantities {
 real deltanew;
 real OR_com;
 real OR_new;
  deltanew = normal_rng(d, sigma);
  //combined OR
  OR_com = exp(d);
  // OR for a new study
  OR_new = exp(deltanew);
}
