// This is the Stan Program for the British Coal mining disater data.
// Finding the change point


data {
  int<lower=0> N;          // number of observations
  int<lower=0> count[N];         // outcome
  real<lower=0> c1;        // value for prior parameter
  real<lower=0> c2;        // value for prior parameter
  real<lower=0> d1;        // value for prior parameter
  real<lower=0> d2;        // value for prior parameter
  real<lower=0> a1;        // value for prior parameter
  real<lower=0> a2;        // value for prior parameter
}
parameters {
  real<lower=0> theta;      // Poisson mean before change point
  real<lower=0> lambda;     // Poisson mean after change point
  real<lower=0> b1;      //prior parameter
  real<lower=0> b2;     // prior parameter
}
model {
  b1 ~ gamma(c1,d1);
  b2 ~ gamma(c2,d2);
  theta ~ gamma(a1,b1);
  lambda ~ gamma(a2,b2);

  for (i in 1:N) 
  target += poisson_lpmf(count[i] | i < 42 ? theta : lambda);
  //for (i in 1:41)
  //count[i] ~ poisson(theta);
  
  //for (i in 42:N)
  //count[i] ~ poisson(lambda);
}
