// This is the Stan Program for the British Coal mining disater data.
// Finding the change point


data {
  int<lower=0> N;          // number of observations
  int<lower=0> count[N];         // outcome
  real<lower=0> c1;        // value for prior parameter
  real<lower=0> c2;        // value for prior parameter
  real<lower=0> d1;        // value for prior parameter
  real<lower=0> d2;        // value for prior parameter
  real<lower=0> a1;        // value for prior parameter
  real<lower=0> a2;        // value for prior parameter
}
transformed data {
  real log_unif;
  log_unif = -log(N);      // log prior distributin for the change point
}
parameters {
  real<lower=0> theta;      // Poisson mean before change point
  real<lower=0> lambda;     // Poisson mean after change point
  real<lower=0> b1;      //prior parameter
  real<lower=0> b2;     // prior parameter
}
transformed parameters {
  vector[N] lp;
  lp = rep_vector(log_unif, N);
  for (k in 1:N)
    for (i in 1:N)
      lp[k] = lp[k] + poisson_lpmf(count[i] | i < k ? theta : lambda);
}
model {
  b1 ~ gamma(c1,d1);
  b2 ~ gamma(c2,d2);
  theta ~ gamma(a1,b1);
  lambda ~ gamma(a2,b2);

  target += log_sum_exp(lp);
}
