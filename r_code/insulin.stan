//
// This Stan program defines the model for the meta-analysis of beta blocker trials 

data {
  int<lower=0> Num;
  int<lower=0> nt[Num];  // num cases, treatment
  int<lower=0> rt[Num];  // num successes, treatment
  int<lower=0> nc[Num];  // num cases, control
  int<lower=0> rc[Num];  // num successes, control
}

parameters { 
  real mu[Num];         // Effect from the control group
  real delta[Num];      // Difference between treatmet groups in each study
  real d;               // mean treatment effect
  real<lower=0> tau;    // deviation of treatment effects
}
transformed parameters {
  real<lower=0> sigma;
  sigma = sqrt(tau);
}
model {
  d ~ normal(0.0,1000);  // vague prior
	tau ~ inv_gamma(0.001,0.001); // vague prior
   //sigma ~ cauchy(0, 5);  // A different way to specify an value prior for SD 

  for (i in 1:Num){
	rc[i] ~ binomial_logit(nc[i], mu[i]);
	rt[i] ~ binomial_logit(nt[i], mu[i] + delta[i]);
	mu[i] ~ normal(0.0, 1000);
	delta[i] ~ normal(d, sigma);
  }
}
generated quantities {
 real deltanew;
 real OR_com;
 real OR_new;
  deltanew = normal_rng(d, sigma);
  //combined OR
  OR_com = exp(d);
  // OR for a new study
  OR_new = exp(deltanew);
}
